// Команда


$('.command li').click(function(e) {
    e.preventDefault();
    var box = $(this).closest('.command');

    box.find('li').removeClass('active');
    $(this).addClass('active');
});


$(".btn-modal").fancybox({
    'padding'    : 0,

    'tpl'        : {
        closeBtn : '<a title="Close" class="btn_close" href="javascript:;"></a>'
    }
});


$('.review').slick({
    autoplay: false,
    dots: false,
    arrows: true,
    mobileFirst: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: '<span class="review-nav prev">',
    nextArrow: '<span class="review-nav next">',
    responsive: [
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 1030,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 1230,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        }
    ]
});

// 1

$('.p1').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    asNavFor: '.p-nav1'
});
$('.p-nav1').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    asNavFor: '.p1',
    dots: false,
    arrows: false,
    centerMode: false,
    focusOnSelect: true
});

$('.p1_1').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    prevArrow: '<span class="portfolio-nav-sub prev">',
    nextArrow: '<span class="portfolio-nav-sub next">'
});

$('.p1_2').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    prevArrow: '<span class="portfolio-nav-sub prev">',
    nextArrow: '<span class="portfolio-nav-sub next">'
});

$('.p1_3').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    prevArrow: '<span class="portfolio-nav-sub prev">',
    nextArrow: '<span class="portfolio-nav-sub next">'
});


$('.p1_4').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    prevArrow: '<span class="portfolio-nav-sub prev">',
    nextArrow: '<span class="portfolio-nav-sub next">'
});


$('.p1_5').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    prevArrow: '<span class="portfolio-nav-sub prev">',
    nextArrow: '<span class="portfolio-nav-sub next">'
});


$('.p1_6').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    prevArrow: '<span class="portfolio-nav-sub prev">',
    nextArrow: '<span class="portfolio-nav-sub next">'
});




// 2

$('.p2').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    prevArrow: '<span class="portfolio-nav prev">',
    nextArrow: '<span class="portfolio-nav next">',
    asNavFor: '.p-nav2'
});
$('.p-nav2').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    asNavFor: '.p2',
    dots: false,
    arrows: false,
    centerMode: false,
    focusOnSelect: true
});

// 3

$('.p3').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    prevArrow: '<span class="portfolio-nav prev">',
    nextArrow: '<span class="portfolio-nav next">',
    asNavFor: '.p-nav3'
});
$('.p-nav3').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    asNavFor: '.p3',
    dots: false,
    arrows: false,
    centerMode: false,
    focusOnSelect: true
});


// 4

$('.p4').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    prevArrow: '<span class="portfolio-nav prev">',
    nextArrow: '<span class="portfolio-nav next">',
    asNavFor: '.p-nav4'
});
$('.p-nav4').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    asNavFor: '.p4',
    dots: false,
    arrows: false,
    centerMode: false,
    focusOnSelect: true
});

// 5

$('.p5').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    prevArrow: '<span class="portfolio-nav prev">',
    nextArrow: '<span class="portfolio-nav next">',
    asNavFor: '.p-nav5'
});
$('.p-nav5').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    asNavFor: '.p5',
    dots: false,
    arrows: false,
    centerMode: false,
    focusOnSelect: true
});


// 6

$('.p6').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    prevArrow: '<span class="portfolio-nav prev">',
    nextArrow: '<span class="portfolio-nav next">',
    asNavFor: '.p-nav6'
});
$('.p-nav6').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    asNavFor: '.p6',
    dots: false,
    arrows: false,
    centerMode: false,
    focusOnSelect: true
});

// 7

$('.p7').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    prevArrow: '<span class="portfolio-nav prev">',
    nextArrow: '<span class="portfolio-nav next">',
    asNavFor: '.p-nav7'
});

$('.p-nav7').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    asNavFor: '.p7',
    dots: false,
    arrows: false,
    centerMode: false,
    focusOnSelect: true
});

/* Portfolio */


$('.tabs-nav li a').click(function(e) {
    e.preventDefault();
    var tab = $($(this).attr("data-target"));
    var box = $(this).closest('.tabs');

    console.log(tab);
    $(this).closest('.tabs-nav').find('li').removeClass('active');
    $(this).closest('li').addClass('active');

    box.find('.tabs-item').removeClass('active');
    box.find(tab).addClass('active');
});


